<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('register', 'AuthController@register');
$router->post('login', 'AuthController@login');
Route::get('logout', 'AuthController@logout');

$router->group(['prefix' => 'api'], function($router) {
    $router->get('articles', 'ArticleController@showAllArticle');
    $router->get('articles/{id}', 'ArticleController@showOneArticle');
    $router->post('articles', 'ArticleController@create');
    $router->put('articles/{id}', 'ArticleController@update');
    $router->get('topic', 'TopicController@showAllTopic');
    $router->get('topic/{id}', 'TopicController@showOneTopic');
    $router->post('topic', 'TopicController@create');
    $router->put('topic/{id}', 'TopicController@update');
    });
  