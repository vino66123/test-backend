<?php

namespace App\Http\Controllers;

use App\Topic;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    public function showAlltopic(){
        return response()->json(Topic::all());
    }

    public function showOnetopic($id){
        return response()->json(Topic::find($id));
    }

    public function create(Request $request){
        //validation

        $this->validate($request, [
            'topic_name' => 'required'
        ]);

        // insert record
        $topic = Topic::create($request->all());
        return response()->json($topic, 201);
    }

    public function update($id, Request $request){
         //validation
         $this->validate($request, [
            'topic_name' => '',
            'slug' => ''
        ]);

        // update record
        $topic = Topic::findOrFail($id);
        $topic->update($request->all());
        return response()->json($topic, 200);
    }

    public function delete($id){
        Topic::findOrFail($id)->delete();
        return response('Deleted successfully', 200);
    }
}