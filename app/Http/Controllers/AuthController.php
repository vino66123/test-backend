<?php
namespace App\Http\Controllers;

use App\User, Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class AuthController extends Controller
{
    public function register(Request $req)
    {
        $this->validate($req, [
            'email' => 'required|unique:users'
        ]);

        $name = $req->input('name');
        $email = $req->input('email');
        $password = Hash::make($req->input('password'));

        $register = User::create([
            'name' => $name,
            'email' => $email,
            'password' =>$password,
        ]);

        if($register) {
            return $req;
        }else{
            return $req;
        }
       
    }

    public function login(Request $req){
        $e = $req->input('email');
        $p = $req->input('password');

        $u = User::where('email', $e)->first();

        if($u AND Hash::check($p, $u->password)){
            $api_token = base64_encode(Str::random(40));

            User::where('email', $e)->update([
                'api_token' => $api_token
            ]);

            // return $api_token;
            return response()->json([
                'data' =>[
                    'user' => $u,
                    'status' => True,
                    'api_token' =>$api_token
                ]
            ]);
        }else{
            return "CAN'T ACCESS";
        }
        
    }

    public function logout(Request $request)
    {        
        if (Auth::check()) {
            $token = Auth::user()->token();
            $token->revoke();
            return $this->sendResponse(null, 'User is logout');
        } 
        else{ 
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised'] , Response::HTTP_UNAUTHORIZED);
        } 
    }
}

